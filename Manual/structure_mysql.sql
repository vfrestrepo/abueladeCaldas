

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
--  Table structure for `evento`
-- ----------------------------
DROP TABLE IF EXISTS `evento`;
CREATE TABLE `evento` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `punto_id` int(11) DEFAULT NULL,
  `nombre` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `descripcion` longtext COLLATE utf8_unicode_ci NOT NULL,
  `fecha` date NOT NULL,
  `hora` time DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_47860B05B3CB6227` (`punto_id`),
  CONSTRAINT `FK_47860B05B3CB6227` FOREIGN KEY (`punto_id`) REFERENCES `punto` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
--  Table structure for `foto`
-- ----------------------------
DROP TABLE IF EXISTS `foto`;
CREATE TABLE `foto` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `punto_id` int(11) DEFAULT NULL,
  `descripcion` text COLLATE utf8_unicode_ci,
  `imagen` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `aprobado` tinyint(1) DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `IDX_EADC3BE5B3CB6227` (`punto_id`),
  CONSTRAINT `FK_EADC3BE5B3CB6227` FOREIGN KEY (`punto_id`) REFERENCES `punto` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
--  Table structure for `foto_evento`
-- ----------------------------
DROP TABLE IF EXISTS `foto_evento`;
CREATE TABLE `foto_evento` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `evento_id` int(11) DEFAULT NULL,
  `descripcion` text COLLATE utf8_unicode_ci,
  `imagen` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `aprobado` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `IDX_7457A15C87A5F842` (`evento_id`),
  CONSTRAINT `FK_7457A15C87A5F842` FOREIGN KEY (`evento_id`) REFERENCES `evento` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
--  Table structure for `fotos_naturaleza`
-- ----------------------------
DROP TABLE IF EXISTS `fotos_naturaleza`;
CREATE TABLE `fotos_naturaleza` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `imagen` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
--  Table structure for `punto`
-- ----------------------------
DROP TABLE IF EXISTS `punto`;
CREATE TABLE `punto` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `descripcion` text COLLATE utf8_unicode_ci,
  `nombre_presidente` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `fecha_fundacion` date DEFAULT NULL,
  `historia` text COLLATE utf8_unicode_ci,
  `nombre_centro_educativo` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `lugar_cultural` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `actos_culturales` text COLLATE utf8_unicode_ci,
  `delimitacion_geografica` text COLLATE utf8_unicode_ci,
  `habitantes` int(11) DEFAULT NULL,
  `ruta` text COLLATE utf8_unicode_ci,
  `is_rural` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

SET FOREIGN_KEY_CHECKS = 1;
