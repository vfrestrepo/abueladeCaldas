
-- ----------------------------
--  Sequence structure for evento_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."evento_id_seq";
CREATE SEQUENCE "public"."evento_id_seq" INCREMENT 1 START 1 MAXVALUE 9223372036854775807 MINVALUE 1 CACHE 1;
ALTER TABLE "public"."evento_id_seq" OWNER TO "turismo";

-- ----------------------------
--  Sequence structure for foto_evento_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."foto_evento_id_seq";
CREATE SEQUENCE "public"."foto_evento_id_seq" INCREMENT 1 START 1 MAXVALUE 9223372036854775807 MINVALUE 1 CACHE 1;
ALTER TABLE "public"."foto_evento_id_seq" OWNER TO "turismo";

-- ----------------------------
--  Sequence structure for foto_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."foto_id_seq";
CREATE SEQUENCE "public"."foto_id_seq" INCREMENT 1 START 1 MAXVALUE 9223372036854775807 MINVALUE 1 CACHE 1;
ALTER TABLE "public"."foto_id_seq" OWNER TO "turismo";

-- ----------------------------
--  Sequence structure for fotos_naturaleza_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."fotos_naturaleza_id_seq";
CREATE SEQUENCE "public"."fotos_naturaleza_id_seq" INCREMENT 1 START 1 MAXVALUE 9223372036854775807 MINVALUE 1 CACHE 1;
ALTER TABLE "public"."fotos_naturaleza_id_seq" OWNER TO "turismo";

-- ----------------------------
--  Sequence structure for punto_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."punto_id_seq";
CREATE SEQUENCE "public"."punto_id_seq" INCREMENT 1 START 1 MAXVALUE 9223372036854775807 MINVALUE 1 CACHE 1;
ALTER TABLE "public"."punto_id_seq" OWNER TO "turismo";

-- ----------------------------
--  Table structure for foto_evento
-- ----------------------------
DROP TABLE IF EXISTS "public"."foto_evento";
CREATE TABLE "public"."foto_evento" (
	"id" int4 NOT NULL,
	"evento_id" int4,
	"descripcion" text COLLATE "default",
	"imagen" varchar(200) DEFAULT NULL::character varying COLLATE "default",
	"aprobado" bool NOT NULL DEFAULT true
)
WITH (OIDS=FALSE);
ALTER TABLE "public"."foto_evento" OWNER TO "turismo";

-- ----------------------------
--  Table structure for fotos_naturaleza
-- ----------------------------
DROP TABLE IF EXISTS "public"."fotos_naturaleza";
CREATE TABLE "public"."fotos_naturaleza" (
	"id" int4 NOT NULL,
	"imagen" varchar(200) DEFAULT NULL::character varying COLLATE "default"
)
WITH (OIDS=FALSE);
ALTER TABLE "public"."fotos_naturaleza" OWNER TO "turismo";

-- ----------------------------
--  Table structure for punto
-- ----------------------------
DROP TABLE IF EXISTS "public"."punto";
CREATE TABLE "public"."punto" (
	"id" int4 NOT NULL DEFAULT nextval('punto_id_seq'::regclass),
	"nombre" varchar(45) DEFAULT NULL::character varying COLLATE "default",
	"descripcion" text COLLATE "default",
	"nombre_presidente" varchar(45) DEFAULT NULL::character varying COLLATE "default",
	"fecha_fundacion" date,
	"historia" text COLLATE "default",
	"nombre_centro_educativo" varchar(45) DEFAULT NULL::character varying COLLATE "default",
	"lugar_cultural" varchar(45) DEFAULT NULL::character varying COLLATE "default",
	"actos_culturales" text COLLATE "default",
	"delimitacion_geografica" text COLLATE "default",
	"habitantes" int4,
	"ruta" text COLLATE "default",
	"is_rural" bool
)
WITH (OIDS=FALSE);
ALTER TABLE "public"."punto" OWNER TO "turismo";

-- ----------------------------
--  Table structure for foto
-- ----------------------------
DROP TABLE IF EXISTS "public"."foto";
CREATE TABLE "public"."foto" (
	"id" int4 NOT NULL,
	"punto_id" int4,
	"descripcion" text COLLATE "default",
	"imagen" varchar(200) DEFAULT NULL::character varying COLLATE "default",
	"aprobado" bool DEFAULT true
)
WITH (OIDS=FALSE);
ALTER TABLE "public"."foto" OWNER TO "turismo";

-- ----------------------------
--  Table structure for evento
-- ----------------------------
DROP TABLE IF EXISTS "public"."evento";
CREATE TABLE "public"."evento" (
	"id" int4 NOT NULL,
	"punto_id" int4,
	"nombre" varchar(100) NOT NULL COLLATE "default",
	"descripcion" text NOT NULL COLLATE "default",
	"fecha" date NOT NULL,
	"hora" time DEFAULT NULL::time without time zone
)
WITH (OIDS=FALSE);
ALTER TABLE "public"."evento" OWNER TO "turismo";


-- ----------------------------
--  Alter sequences owned by
-- ----------------------------
ALTER SEQUENCE "public"."evento_id_seq" RESTART 2;
ALTER SEQUENCE "public"."foto_evento_id_seq" RESTART 2;
ALTER SEQUENCE "public"."foto_id_seq" RESTART 2;
ALTER SEQUENCE "public"."fotos_naturaleza_id_seq" RESTART 2;
ALTER SEQUENCE "public"."punto_id_seq" RESTART 2 OWNED BY "punto"."id";
-- ----------------------------
--  Primary key structure for table foto_evento
-- ----------------------------
ALTER TABLE "public"."foto_evento" ADD PRIMARY KEY ("id") NOT DEFERRABLE INITIALLY IMMEDIATE;

-- ----------------------------
--  Indexes structure for table foto_evento
-- ----------------------------
CREATE INDEX  "idx_7457a15c87a5f842" ON "public"."foto_evento" USING btree(evento_id "pg_catalog"."int4_ops" ASC NULLS LAST);

-- ----------------------------
--  Primary key structure for table fotos_naturaleza
-- ----------------------------
ALTER TABLE "public"."fotos_naturaleza" ADD PRIMARY KEY ("id") NOT DEFERRABLE INITIALLY IMMEDIATE;

-- ----------------------------
--  Primary key structure for table punto
-- ----------------------------
ALTER TABLE "public"."punto" ADD PRIMARY KEY ("id") NOT DEFERRABLE INITIALLY IMMEDIATE;

-- ----------------------------
--  Primary key structure for table foto
-- ----------------------------
ALTER TABLE "public"."foto" ADD PRIMARY KEY ("id") NOT DEFERRABLE INITIALLY IMMEDIATE;

-- ----------------------------
--  Indexes structure for table foto
-- ----------------------------
CREATE INDEX  "idx_eadc3be5b3cb6227" ON "public"."foto" USING btree(punto_id "pg_catalog"."int4_ops" ASC NULLS LAST);

-- ----------------------------
--  Primary key structure for table evento
-- ----------------------------
ALTER TABLE "public"."evento" ADD PRIMARY KEY ("id") NOT DEFERRABLE INITIALLY IMMEDIATE;

-- ----------------------------
--  Indexes structure for table evento
-- ----------------------------
CREATE INDEX  "idx_47860b05b3cb6227" ON "public"."evento" USING btree(punto_id "pg_catalog"."int4_ops" ASC NULLS LAST);

-- ----------------------------
--  Foreign keys structure for table foto_evento
-- ----------------------------
ALTER TABLE "public"."foto_evento" ADD CONSTRAINT "fk_7457a15c87a5f842" FOREIGN KEY ("evento_id") REFERENCES "public"."evento" ("id") ON UPDATE NO ACTION ON DELETE NO ACTION NOT DEFERRABLE INITIALLY IMMEDIATE;

-- ----------------------------
--  Foreign keys structure for table foto
-- ----------------------------
ALTER TABLE "public"."foto" ADD CONSTRAINT "fk_eadc3be5b3cb6227" FOREIGN KEY ("punto_id") REFERENCES "public"."punto" ("id") ON UPDATE NO ACTION ON DELETE NO ACTION NOT DEFERRABLE INITIALLY IMMEDIATE;

-- ----------------------------
--  Foreign keys structure for table evento
-- ----------------------------
ALTER TABLE "public"."evento" ADD CONSTRAINT "fk_47860b05b3cb6227" FOREIGN KEY ("punto_id") REFERENCES "public"."punto" ("id") ON UPDATE NO ACTION ON DELETE NO ACTION NOT DEFERRABLE INITIALLY IMMEDIATE;

