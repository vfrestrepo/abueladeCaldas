<?php

namespace Turismo\TurismoBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TimeType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class EventoType extends AbstractType
{
	/**
	 * @param FormBuilderInterface $builder
	 * @param array                $options
	 */
	public function buildForm (FormBuilderInterface $builder, array $options)
	{
		$builder
			->add ('nombre')
			->add ('descripcion')
			->add ('fecha', DateType::class, ['widget' => 'single_text',
			                                    'format' => 'yyyy-MM-dd'])
			->add ('hora', TimeType::class)
			->add ('punto')
			;
		;
	}

	/**
	 * @param OptionsResolver $resolver
	 */
	public function configureOptions (OptionsResolver $resolver)
	{
		$resolver->setDefaults ([
			'data_class' => 'Turismo\TurismoBundle\Entity\Evento',
			'allow_extra_fields' => TRUE
		]);
	}
}
