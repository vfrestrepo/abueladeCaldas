<?php

namespace Turismo\TurismoBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\OptionsResolver\OptionsResolver;

class FotoType extends AbstractType
{
	/**
	 * @param FormBuilderInterface $builder
	 * @param array                $options
	 */
	public function buildForm (FormBuilderInterface $builder, array $options)
	{
		$builder
			->add ('imagen', FileType::class, [
				'label' => 'Imagen',
			])
			->add ('descripcion', TextareaType::class)
			->add ('punto');
	}

	/**
	 * @param OptionsResolver $resolver
	 */
	public function configureOptions (OptionsResolver $resolver)
	{
		$resolver->setDefaults ([
			'data_class' => 'Turismo\TurismoBundle\Entity\Foto'
		]);
	}
}
