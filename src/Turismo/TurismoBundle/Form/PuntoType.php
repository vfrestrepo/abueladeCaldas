<?php

namespace Turismo\TurismoBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class PuntoType extends AbstractType
{
	/**
	 * @param FormBuilderInterface $builder
	 * @param array                $options
	 */
	public function buildForm (FormBuilderInterface $builder, array $options)
	{
		$builder
			->add ('nombre')
			->add ('descripcion')
			->add ('nombrePresidente')
			->add ('fechaFundacion', DateType::class, ['widget' => 'single_text',
			                                           'format' => 'yyyy-MM-dd'])
			->add ('historia')
			->add ('nombreCentroEducativo')
			->add ('lugarCultural')
			->add ('actosCulturales')
			->add ('delimitacionGeografica')
			->add ('habitantes')
			->add ('ruta')
			->add ('isRural');

	}

	/**
	 * @param OptionsResolver $resolver
	 */
	public function configureOptions (OptionsResolver $resolver)
	{
		$resolver->setDefaults ([
			'data_class' => 'Turismo\TurismoBundle\Entity\Punto'
		]);
	}
}
