<?php

namespace Turismo\TurismoBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class FotoEventoType extends AbstractType
{
	/**
	 * @param FormBuilderInterface $builder
	 * @param array                $options
	 */
	public function buildForm (FormBuilderInterface $builder, array $options)
	{
		$builder
			->add ('descripcion', TextareaType::class)
			->add ('imagen', FileType::class, [
				'label' => 'Imagen',
			])
			->add ('evento');
	}

	/**
	 * @param OptionsResolver $resolver
	 */
	public function configureOptions (OptionsResolver $resolver)
	{
		$resolver->setDefaults ([
			'data_class' => 'Turismo\TurismoBundle\Entity\FotoEvento'
		]);
	}
}
