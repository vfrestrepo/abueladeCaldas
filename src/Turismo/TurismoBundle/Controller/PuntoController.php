<?php

namespace Turismo\TurismoBundle\Controller;

use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Turismo\TurismoBundle\Entity\Foto;
use Turismo\TurismoBundle\Entity\Punto;
use Turismo\TurismoBundle\Form\PuntoType;

/**
 * Punto controller.
 *
 * @Route("/admin/punto")
 */
class PuntoController extends Controller
{
	/**
	 * Lists all Punto entities.
	 *
	 * @Route("/", name="admin_punto_index")
	 * @Method("GET")
	 */
	public function indexAction ()
	{
		$em = $this->getDoctrine ()->getManager ();

		$puntos = $em->getRepository ('TurismoBundle:Punto')->findAll ();

		return $this->render ('punto/index.html.twig', [
			'puntos' => $puntos,
		]);
	}

	/**
	 * Creates a new Punto entity.
	 *
	 * @Route("/new", name="admin_punto_new")
	 * @Method({"GET", "POST"})
	 */
	public function newAction (Request $request)
	{
		$punto = new Punto();

		$form = $this->createForm ('Turismo\TurismoBundle\Form\PuntoType', $punto);
		$form->add ('addImage', SubmitType::class, ['label' => 'Agregar Imagen',
		                                            'attr'  => ['class' => 'btn btn-info']]);
		$form->handleRequest ($request);


		if ($form->isSubmitted () && $form->isValid ()) {

			$em = $this->getDoctrine ()->getManager ();
			$em->persist ($punto);
			$em->flush ();
			if ($form->get ('addImage')->isClicked ()) {
				return $this->redirectToRoute ('admin_foto_new', ['idPunto' => $punto->getId ()]);
			} else {
				return $this->redirectToRoute ('admin_punto_show', ['id' => $punto->getId ()]);
			}

		}

		return $this->render ('punto/new.html.twig', [
			'punto' => $punto,
			'form'  => $form->createView (),
		]);
	}

	/**
	 * Finds and displays a Punto entity.
	 *
	 * @Route("/{id}", name="admin_punto_show")
	 * @Method("GET")
	 */
	public function showAction (Punto $punto)
	{
		$deleteForm = $this->createDeleteForm ($punto);

		return $this->render ('punto/show.html.twig', [
			'punto'       => $punto,
			'delete_form' => $deleteForm->createView (),
		]);
	}

	/**
	 * Displays a form to edit an existing Punto entity.
	 *
	 * @Route("/{id}/edit", name="admin_punto_edit")
	 * @Method({"GET", "POST"})
	 */
	public function editAction (Request $request, Punto $punto)
	{
		$deleteForm = $this->createDeleteForm ($punto);
		$editForm = $this->createForm ('Turismo\TurismoBundle\Form\PuntoType', $punto);
		$editForm->handleRequest ($request);

		if ($editForm->isSubmitted () && $editForm->isValid ()) {
			$em = $this->getDoctrine ()->getManager ();
			$em->persist ($punto);
			$em->flush ();

			return $this->redirectToRoute ('admin_punto_edit', ['id' => $punto->getId ()]);
		}

		return $this->render ('punto/edit.html.twig', [
			'punto'       => $punto,
			'edit_form'   => $editForm->createView (),
			'delete_form' => $deleteForm->createView (),
		]);
	}

	/**
	 * Deletes a Punto entity.
	 *
	 * @Route("/{id}", name="admin_punto_delete")
	 * @Method("DELETE")
	 */
	public function deleteAction (Request $request, Punto $punto)
	{
		$form = $this->createDeleteForm ($punto);
		$form->handleRequest ($request);

		if ($form->isSubmitted () && $form->isValid ()) {
			$em = $this->getDoctrine ()->getManager ();
			$em->remove ($punto);
			$em->flush ();
		}

		return $this->redirectToRoute ('admin_punto_index');
	}

	/**
	 * Creates a form to delete a Punto entity.
	 *
	 * @param Punto $punto The Punto entity
	 *
	 * @return \Symfony\Component\Form\Form The form
	 */
	private function createDeleteForm (Punto $punto)
	{
		return $this->createFormBuilder ()
			->setAction ($this->generateUrl ('admin_punto_delete', ['id' => $punto->getId ()]))
			->setMethod ('DELETE')
			->getForm ();
	}
}
