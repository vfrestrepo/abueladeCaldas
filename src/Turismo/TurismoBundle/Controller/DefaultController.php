<?php

namespace Turismo\TurismoBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Turismo\TurismoBundle\Entity\Evento;
use Turismo\TurismoBundle\Entity\FotosNaturaleza;
use Turismo\TurismoBundle\Entity\Punto;


/**
 * Class DefaultController
 *
 * @package Turismo\TurismoBundle\Controller
 *
 */
class DefaultController extends Controller
{
	/**
	 * @Route("/")
	 */
	public function indexAction ()
	{
		$em = $this->getDoctrine ()->getManager ();

		$puntos = $em->getRepository ('TurismoBundle:Punto')->findAll ();

		return $this->render ('base.html.twig', [
			'puntos' => $puntos,
		]);
	}

	/**
	 * @Route("/{id}", requirements={"id": "\d+"})
	 * @Method("GET")
	 * @param \Turismo\TurismoBundle\Entity\Punto $punto
	 *
	 * @return \Symfony\Component\HttpFoundation\Response
	 */
	public function viewAction (Punto $punto)
	{
		return $this->render ('@Turismo/Default/viewItem.html.twig', [
			'punto' => $punto,
		]);
	}

	/**
	 * @Route("/viewAll")
	 */
	public function viewAllAction ()
	{
		$em = $this->getDoctrine ()->getManager ();

		$puntos = $em->getRepository (Punto::class)->findAll ();

		return $this->render ('@Turismo/Default/viewAllItems.html.twig', [
			'puntos' => $puntos,
		]);
	}

	/**
	 * @Route("/event/{id}", requirements={"id": "\d+"})
	 * @Method("GET")
	 * @param \Turismo\TurismoBundle\Entity\Punto $punto
	 *
	 * @return \Symfony\Component\HttpFoundation\Response
	 */
	public function viewEventAction (Punto $punto)
	{
		return $this->render ('@Turismo/Default/viewItem.html.twig', [
			'punto' => $punto,
		]);
	}

	/**
	 * @Route("/event/All")
	 */
	public function viewAllEventsAction ()
	{
		$em = $this->getDoctrine ()->getManager ();

		$eventos = $em->getRepository (Evento::class)->findAll ();

		return $this->render ('@Turismo/Default/viewAllEvents.html.twig', [
			'eventos' => $eventos,
		]);
	}
	/**
	 * @Route("/admin/home")
	 */
	public function homeAction ()
	{
		return $this->render ('@Turismo/Default/home.html.twig');
	}


	/**
	 * @Route("/history")
	 */
	public function historyAction ()
	{
		return $this->render ('@Turismo/Default/historia.html.twig');
	}

	/**
	 * @Route("/naturaleza")
	 */
	public function naturalezaAction ()
	{
		$em = $this->getDoctrine ()->getManager ();

		$fotos = $em->getRepository (FotosNaturaleza::class)->findAll ();

		return $this->render ('@Turismo/Default/naturaleza.html.twig', [
			'fotos' => $fotos,
		]);
	}

	/**
	 * @Route("/cultura")
	 */
	public function culturaAction ()
	{
		return $this->render ('@Turismo/Default/cultura.html.twig');
	}
}
