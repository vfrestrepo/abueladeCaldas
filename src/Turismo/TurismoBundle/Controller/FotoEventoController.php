<?php

namespace Turismo\TurismoBundle\Controller;

use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Turismo\TurismoBundle\Entity\Evento;
use Turismo\TurismoBundle\Entity\FotoEvento;
use Turismo\TurismoBundle\Form\FotoEventoType;

/**
 * FotoEvento controller.
 *
 * @Route("/admin/fotoevento")
 */
class FotoEventoController extends Controller
{
	/**
	 * Lists all FotoEvento entities.
	 *
	 * @Route("/", name="admin_fotoevento_index")
	 * @Method("GET")
	 */
	public function indexAction ()
	{
		$em = $this->getDoctrine ()->getManager ();

		$fotoEventos = $em->getRepository ('TurismoBundle:FotoEvento')->findAll ();

		return $this->render ('fotoevento/index.html.twig', [
			'fotoEventos' => $fotoEventos,
		]);
	}

	/**
	 * Creates a new FotoEvento entity.
	 *
	 * @Route("/new", name="admin_fotoevento_new")
	 * @Method({"GET", "POST"})
	 */
	public function newAction (Request $request)
	{
		$fotoEvento = new FotoEvento();

		$idEvento = $request->get ('idEvento');
		if (NULL != $idEvento) {
			$evento = $this->getDoctrine ()
				->getRepository ('TurismoBundle:Evento')
				->find ($idEvento);
			$fotoEvento->setEvento ($evento);
		}

		$form = $this->createForm ('Turismo\TurismoBundle\Form\FotoEventoType', $fotoEvento);
		$form->handleRequest ($request);

		if ($form->isSubmitted () && $form->isValid ()) {

			/**
			 * @var $imagen UploadedFile
			 */
			$imagen = $fotoEvento->getImagen ();
			$nombre = $this->get ('turismo.file_uploader')->upload ($imagen);

			$fotoEvento->setImagen ($nombre);
			$fotoEvento->setAprobado(TRUE);

			$em = $this->getDoctrine ()->getManager ();
			$em->persist ($fotoEvento);
			$em->flush ();

			return $this->redirectToRoute ('admin_fotoevento_show', ['id' => $fotoEvento->getId ()]);
		}

		return $this->render ('fotoevento/new.html.twig', [
			'fotoEvento' => $fotoEvento,
			'form'       => $form->createView (),
		]);
	}

	/**
	 * Finds and displays a FotoEvento entity.
	 *
	 * @Route("/{id}", name="admin_fotoevento_show")
	 * @Method("GET")
	 */
	public function showAction (FotoEvento $fotoEvento)
	{
		$deleteForm = $this->createDeleteForm ($fotoEvento);

		return $this->render ('fotoevento/show.html.twig', [
			'fotoEvento'  => $fotoEvento,
			'delete_form' => $deleteForm->createView (),
		]);
	}

	/**
	 * Displays a form to edit an existing FotoEvento entity.
	 *
	 * @Route("/{id}/edit", name="admin_fotoevento_edit")
	 * @Method({"GET", "POST"})
	 */
	public function editAction (Request $request, FotoEvento $fotoEvento)
	{
		$fotoEvento->setImagen (new File($this->getParameter ('album') . '/' . $fotoEvento->getImagen ()));
		$deleteForm = $this->createDeleteForm ($fotoEvento);
		$editForm = $this->createForm ('Turismo\TurismoBundle\Form\FotoEventoType', $fotoEvento);
		$editForm->handleRequest ($request);

		if ($editForm->isSubmitted () && $editForm->isValid ()) {
			$em = $this->getDoctrine ()->getManager ();
			$em->persist ($fotoEvento);
			$em->flush ();

			return $this->redirectToRoute ('admin_fotoevento_edit', ['id' => $fotoEvento->getId ()]);
		}

		return $this->render ('fotoevento/edit.html.twig', [
			'fotoEvento'  => $fotoEvento,
			'edit_form'   => $editForm->createView (),
			'delete_form' => $deleteForm->createView (),
		]);
	}

	/**
	 * Deletes a FotoEvento entity.
	 *
	 * @Route("/{id}", name="admin_fotoevento_delete")
	 * @Method("DELETE")
	 */
	public function deleteAction (Request $request, FotoEvento $fotoEvento)
	{
		$form = $this->createDeleteForm ($fotoEvento);
		$form->handleRequest ($request);

		if ($form->isSubmitted () && $form->isValid ()) {
			$em = $this->getDoctrine ()->getManager ();
			$em->remove ($fotoEvento);
			$em->flush ();
		}

		return $this->redirectToRoute ('admin_fotoevento_index');
	}

	/**
	 * Creates a form to delete a FotoEvento entity.
	 *
	 * @param FotoEvento $fotoEvento The FotoEvento entity
	 *
	 * @return \Symfony\Component\Form\Form The form
	 */
	private function createDeleteForm (FotoEvento $fotoEvento)
	{
		return $this->createFormBuilder ()
			->setAction ($this->generateUrl ('admin_fotoevento_delete', ['id' => $fotoEvento->getId ()]))
			->setMethod ('DELETE')
			->getForm ();
	}
}
