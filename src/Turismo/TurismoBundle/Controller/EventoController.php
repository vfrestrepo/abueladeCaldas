<?php

namespace Turismo\TurismoBundle\Controller;

use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Turismo\TurismoBundle\Entity\Evento;
use Turismo\TurismoBundle\Form\EventoType;

/**
 * Evento controller.
 *
 * @Route("/admin/evento")
 */
class EventoController extends Controller
{
	/**
	 * Lists all Evento entities.
	 *
	 * @Route("/", name="admin_evento_index")
	 * @Method("GET")
	 */
	public function indexAction ()
	{
		$em = $this->getDoctrine ()->getManager ();

		$eventos = $em->getRepository ('TurismoBundle:Evento')->findAll ();

		return $this->render ('evento/index.html.twig', [
			'eventos' => $eventos,
		]);
	}

	/**
	 * Creates a new Evento entity.
	 *
	 * @Route("/new", name="admin_evento_new")
	 * @Method({"GET", "POST"})
	 */
	public function newAction (Request $request)
	{
		$evento = new Evento();
		$form = $this->createForm ('Turismo\TurismoBundle\Form\EventoType', $evento);
		$form->add ('addImage', SubmitType::class, ['label' => 'Agregar Imagen',
		                                            'attr'  => ['class' => 'btn btn-info']]);
		$form->handleRequest ($request);

		if ($form->isSubmitted () && $form->isValid ()) {
			$em = $this->getDoctrine ()->getManager ();
			$em->persist ($evento);
			$em->flush ();


			if ($form->get ('addImage')->isClicked ()) {
				return $this->redirectToRoute ('admin_fotoevento_new', ['idEvento' => $evento->getId ()]);
			} else {
				return $this->redirectToRoute ('admin_evento_show', ['id' => $evento->getId ()]);
			}


		}

		return $this->render ('evento/new.html.twig', [
			'evento' => $evento,
			'form'   => $form->createView (),
		]);
	}

	/**
	 * Finds and displays a Evento entity.
	 *
	 * @Route("/{id}", name="admin_evento_show")
	 * @Method("GET")
	 */
	public function showAction (Evento $evento)
	{
		$deleteForm = $this->createDeleteForm ($evento);

		return $this->render ('evento/show.html.twig', [
			'evento'      => $evento,
			'delete_form' => $deleteForm->createView (),
		]);
	}

	/**
	 * Displays a form to edit an existing Evento entity.
	 *
	 * @Route("/{id}/edit", name="admin_evento_edit")
	 * @Method({"GET", "POST"})
	 */
	public function editAction (Request $request, Evento $evento)
	{
		$deleteForm = $this->createDeleteForm ($evento);
		$editForm = $this->createForm ('Turismo\TurismoBundle\Form\EventoType', $evento);
		$editForm->handleRequest ($request);

		if ($editForm->isSubmitted () && $editForm->isValid ()) {
			$em = $this->getDoctrine ()->getManager ();
			$em->persist ($evento);
			$em->flush ();

			return $this->redirectToRoute ('admin_evento_edit', ['id' => $evento->getId ()]);
		}

		return $this->render ('evento/edit.html.twig', [
			'evento'      => $evento,
			'edit_form'   => $editForm->createView (),
			'delete_form' => $deleteForm->createView (),
		]);
	}

	/**
	 * Deletes a Evento entity.
	 *
	 * @Route("/{id}", name="admin_evento_delete")
	 * @Method("DELETE")
	 */
	public function deleteAction (Request $request, Evento $evento)
	{
		$form = $this->createDeleteForm ($evento);
		$form->handleRequest ($request);

		if ($form->isSubmitted () && $form->isValid ()) {
			$em = $this->getDoctrine ()->getManager ();
			$em->remove ($evento);
			$em->flush ();
		}

		return $this->redirectToRoute ('admin_evento_index');
	}

	/**
	 * Creates a form to delete a Evento entity.
	 *
	 * @param Evento $evento The Evento entity
	 *
	 * @return \Symfony\Component\Form\Form The form
	 */
	private function createDeleteForm (Evento $evento)
	{
		return $this->createFormBuilder ()
			->setAction ($this->generateUrl ('admin_evento_delete', ['id' => $evento->getId ()]))
			->setMethod ('DELETE')
			->getForm ();
	}
}
