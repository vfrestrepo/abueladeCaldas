<?php

namespace Turismo\TurismoBundle\Controller;

use Symfony\Component\HttpFoundation\File\UploadedFile;
use Turismo\TurismoBundle\Entity\FotosNaturaleza;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;use Symfony\Component\HttpFoundation\Request;

/**
 * Fotosnaturaleza controller.
 *
 * @Route("admin/naturaleza")
 */
class FotosNaturalezaController extends Controller
{
    /**
     * Lists all fotosNaturaleza entities.
     *
     * @Route("/", name="admin_naturaleza_index")
     * @Method("GET")
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $fotosNaturalezas = $em->getRepository('TurismoBundle:FotosNaturaleza')->findAll();

        return $this->render('fotosnaturaleza/index.html.twig', array(
            'fotosNaturalezas' => $fotosNaturalezas,
        ));
    }

    /**
     * Creates a new fotosNaturaleza entity.
     *
     * @Route("/new", name="admin_naturaleza_new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request)
    {
        $fotosNaturaleza = new FotosNaturaleza();
        $form = $this->createForm('Turismo\TurismoBundle\Form\FotosNaturalezaType', $fotosNaturaleza);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

	        /**
	         * @var $imagen UploadedFile
	         */
	        $imagen = $fotosNaturaleza->getImagen ();
	        $nombre = $this->get ('turismo.file_uploader')->upload ($imagen);

	        $fotosNaturaleza->setImagen ($nombre);

            $em = $this->getDoctrine()->getManager();
            $em->persist($fotosNaturaleza);
            $em->flush($fotosNaturaleza);

            return $this->redirectToRoute('admin_naturaleza_show', array('id' => $fotosNaturaleza->getId()));
        }

        return $this->render('fotosnaturaleza/new.html.twig', array(
            'fotosNaturaleza' => $fotosNaturaleza,
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a fotosNaturaleza entity.
     *
     * @Route("/{id}", name="admin_naturaleza_show")
     * @Method("GET")
     */
    public function showAction(FotosNaturaleza $fotosNaturaleza)
    {
        $deleteForm = $this->createDeleteForm($fotosNaturaleza);

        return $this->render('fotosnaturaleza/show.html.twig', array(
            'fotosNaturaleza' => $fotosNaturaleza,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing fotosNaturaleza entity.
     *
     * @Route("/{id}/edit", name="admin_naturaleza_edit")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, FotosNaturaleza $fotosNaturaleza)
    {
        $deleteForm = $this->createDeleteForm($fotosNaturaleza);
        $editForm = $this->createForm('Turismo\TurismoBundle\Form\FotosNaturalezaType', $fotosNaturaleza);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('admin_naturaleza_edit', array('id' => $fotosNaturaleza->getId()));
        }

        return $this->render('fotosnaturaleza/edit.html.twig', array(
            'fotosNaturaleza' => $fotosNaturaleza,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a fotosNaturaleza entity.
     *
     * @Route("/{id}", name="admin_naturaleza_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, FotosNaturaleza $fotosNaturaleza)
    {
        $form = $this->createDeleteForm($fotosNaturaleza);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($fotosNaturaleza);
            $em->flush($fotosNaturaleza);
        }

        return $this->redirectToRoute('admin_naturaleza_index');
    }

    /**
     * Creates a form to delete a fotosNaturaleza entity.
     *
     * @param FotosNaturaleza $fotosNaturaleza The fotosNaturaleza entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(FotosNaturaleza $fotosNaturaleza)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('admin_naturaleza_delete', array('id' => $fotosNaturaleza->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }
}
