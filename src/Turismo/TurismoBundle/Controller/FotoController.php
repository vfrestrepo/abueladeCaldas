<?php

namespace Turismo\TurismoBundle\Controller;

use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Turismo\TurismoBundle\Entity\Foto;
use Turismo\TurismoBundle\Entity\Punto;

/**
 * Foto controller.
 *
 * @Route("/admin/foto")
 */
class FotoController extends Controller
{
	/**
	 * Lists all Foto entities.
	 *
	 * @Route("/", name="admin_foto_index")
	 * @Method("GET")
	 */
	public function indexAction ()
	{
		$em = $this->getDoctrine ()->getManager ();

		$fotos = $em->getRepository ('TurismoBundle:Foto')->findAll ();

		return $this->render ('foto/index.html.twig', [
			'fotos' => $fotos,
		]);
	}

	/**
	 * Creates a new Foto entity.
	 *
	 * @Route("/new", name="admin_foto_new")
	 * @Method({"GET", "POST"})
	 */
	public function newAction (Request $request)
	{
		$foto = new Foto();

		$idPunto = $request->get ('idPunto');
		if (NULL != $idPunto) {
			$punto = $this->getDoctrine ()
				->getRepository ('TurismoBundle:Punto')
				->find ($idPunto);
			$foto->setPunto ($punto);
		}
		$form = $this->createForm ('Turismo\TurismoBundle\Form\FotoType', $foto);
		$form->handleRequest ($request);

		if ($form->isSubmitted () && $form->isValid ()) {
			/**
			 * @var $imagen UploadedFile
			 */
			$imagen = $foto->getImagen ();
			$nombre = $this->get ('turismo.file_uploader')->upload ($imagen);

			$foto->setImagen ($nombre);
			$foto->setAprobado(TRUE);

			$em = $this->getDoctrine ()->getManager ();
			$em->persist ($foto);
			$em->flush ();

			return $this->redirectToRoute ('admin_foto_show', ['id' => $foto->getId ()]);
		}

		return $this->render ('foto/new.html.twig', [
			'foto' => $foto,
			'form' => $form->createView (),
		]);
	}


	/**
	 * Finds and displays a Foto entity.
	 *
	 * @Route("/{id}", name="admin_foto_show")
	 * @Method("GET")
	 */
	public function showAction (Foto $foto)
	{
		$deleteForm = $this->createDeleteForm ($foto);

		return $this->render ('foto/show.html.twig', [
			'foto'        => $foto,
			'delete_form' => $deleteForm->createView (),
		]);
	}

	/**
	 * Displays a form to edit an existing Foto entity.
	 *
	 * @Route("/{id}/edit", name="admin_foto_edit")
	 * @Method({"GET", "POST"})
	 */
	public function editAction (Request $request, Foto $foto)
	{
		$foto->setImagen (new File($this->getParameter ('album') . '/' . $foto->getImagen ()));
		$deleteForm = $this->createDeleteForm ($foto);
		$editForm = $this->createForm ('Turismo\TurismoBundle\Form\FotoType', $foto);
		$editForm->handleRequest ($request);

		if ($editForm->isSubmitted () && $editForm->isValid ()) {
			$em = $this->getDoctrine ()->getManager ();
			$em->persist ($foto);
			$em->flush ();

			return $this->redirectToRoute ('admin_foto_edit', ['id' => $foto->getId ()]);
		}

		return $this->render ('foto/edit.html.twig', [
			'foto'        => $foto,
			'edit_form'   => $editForm->createView (),
			'delete_form' => $deleteForm->createView (),
		]);
	}

	/**
	 * Deletes a Foto entity.
	 *
	 * @Route("/{id}", name="admin_foto_delete")
	 * @Method("DELETE")
	 */
	public function deleteAction (Request $request, Foto $foto)
	{
		$form = $this->createDeleteForm ($foto);
		$form->handleRequest ($request);

		if ($form->isSubmitted () && $form->isValid ()) {
			$em = $this->getDoctrine ()->getManager ();
			$em->remove ($foto);
			$em->flush ();
		}

		return $this->redirectToRoute ('admin_foto_index');
	}

	/**
	 * Creates a form to delete a Foto entity.
	 *
	 * @param Foto $foto The Foto entity
	 *
	 * @return \Symfony\Component\Form\Form The form
	 */
	private function createDeleteForm (Foto $foto)
	{
		return $this->createFormBuilder ()
			->setAction ($this->generateUrl ('admin_foto_delete', ['id' => $foto->getId ()]))
			->setMethod ('DELETE')
			->getForm ();
	}
}
