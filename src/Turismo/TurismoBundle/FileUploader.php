<?php

namespace Turismo\TurismoBundle;

use Symfony\Component\HttpFoundation\File\UploadedFile;

class FileUploader
{
	private $targetDir;

	public function __construct ($targetDir)
	{
		$this->targetDir = $targetDir;
	}

	public function upload (UploadedFile $file)
	{
		$fileName = md5 (uniqid ()) . '.' . $file->guessExtension ();

		$file->move ($this->targetDir, $fileName);

		return $fileName;
	}

	/**
	 * @return mixed
	 */
	public function getTargetDir ()
	{
		return $this->targetDir;
	}
}
