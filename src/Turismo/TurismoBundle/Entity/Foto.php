<?php

namespace Turismo\TurismoBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\JoinColumn;
use Doctrine\ORM\Mapping\ManyToOne;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Foto
 *
 * @ORM\Entity
 */
class Foto
{
	/**
	 * @var integer
	 *
	 * @ORM\Column(name="id", type="integer")
	 * @ORM\Id
	 * @ORM\GeneratedValue(strategy="AUTO")
	 */
	private $id;

	/**
	 * @var string
	 *
	 * @ORM\Column(name="descripcion", type="text", length=65535, nullable=true)
	 */
	private $descripcion;

	/**
	 * @var string
	 *
	 * @ORM\Column(name="imagen", type="string", length=200, nullable=true)
	 *
	 * @Assert\Image()
	 *
	 *
	 */
	private $imagen;

	/**
	 * @var boolean
	 *
	 * @ORM\Column(name="aprobado", type="boolean", nullable=true, options={"default":true})
	 */
	private $aprobado;

	/**
	 * @var Punto
	 *
	 * @ManyToOne(targetEntity="Turismo\TurismoBundle\Entity\Punto", inversedBy="fotos")
	 * @JoinColumn(name="punto_id", referencedColumnName="id")
	 *
	 */
	private $punto;


	/**
	 * Set id
	 *
	 * @param integer $id
	 *
	 * @return Foto
	 */
	public function setId ($id)
	{
		$this->id = $id;

		return $this;
	}

	/**
	 * Get id
	 *
	 * @return integer
	 */
	public function getId ()
	{
		return $this->id;
	}

	/**
	 * Set descripcion
	 *
	 * @param string $descripcion
	 *
	 * @return Foto
	 */
	public function setDescripcion ($descripcion)
	{
		$this->descripcion = $descripcion;

		return $this;
	}

	/**
	 * Get descripcion
	 *
	 * @return string
	 */
	public function getDescripcion ()
	{
		return $this->descripcion;
	}

	/**
	 * Set imagen
	 *
	 * @param string $imagen
	 *
	 * @return Foto
	 */
	public function setImagen ($imagen)
	{
		$this->imagen = $imagen;

		return $this;
	}

	/**
	 * Get imagen
	 *
	 * @return string
	 */
	public function getImagen ()
	{
		return $this->imagen;
	}

	/**
	 * Set aprobado
	 *
	 * @param boolean $aprobado
	 *
	 * @return Foto
	 */
	public function setAprobado ($aprobado)
	{
		$this->aprobado = $aprobado;

		return $this;
	}

	/**
	 * Get aprobado
	 *
	 * @return boolean
	 */
	public function getAprobado ()
	{
		return $this->aprobado;
	}

	/**
	 * Set punto
	 *
	 * @param Punto $punto
	 *
	 * @return Foto
	 */
	public function setPunto (Punto $punto = NULL)
	{
		$this->punto = $punto;

		return $this;
	}

	/**
	 * Get punto
	 *
	 * @return Punto
	 */
	public function getPunto ()
	{
		return $this->punto;
	}



}
