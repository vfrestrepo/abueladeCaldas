<?php

namespace Turismo\TurismoBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\JoinColumn;
use Doctrine\ORM\Mapping\ManyToOne;
use Doctrine\ORM\Mapping\OneToMany;

/**
 * Evento
 *
 * @ORM\Table(name="evento")
 * @ORM\Entity(repositoryClass="Turismo\TurismoBundle\Repository\EventoRepository")
 */
class Evento
{
	/**
	 * @var int
	 *
	 * @ORM\Column(name="id", type="integer")
	 * @ORM\Id
	 * @ORM\GeneratedValue(strategy="AUTO")
	 */
	private $id;

	/**
	 * @var string
	 *
	 * @ORM\Column(name="nombre", type="string", length=100)
	 */
	private $nombre;

	/**
	 * @var string
	 *
	 * @ORM\Column(name="descripcion", type="text")
	 */
	private $descripcion;

	/**
	 * @var \DateTime
	 *
	 * @ORM\Column(name="fecha", type="date")
	 */
	private $fecha;

	/**
	 * @var \DateTime
	 *
	 * @ORM\Column(name="hora", type="time", nullable=true)
	 */
	private $hora;

	/**
	 * @var Evento
	 *
	 * @OneToMany(targetEntity="Turismo\TurismoBundle\Entity\FotoEvento", mappedBy="evento")
	 *
	 */
	private $fotos;

	/**
	 * @var Punto
	 *
	 * @ManyToOne(targetEntity="Turismo\TurismoBundle\Entity\Punto", inversedBy="eventos")
	 * @JoinColumn(name="punto_id", referencedColumnName="id")
	 *
	 */
	private $punto;

	/**
	 * @return \Turismo\TurismoBundle\Entity\Punto
	 */
	public function getPunto ()
	{
		return $this->punto;
	}

	/**
	 * @param \Turismo\TurismoBundle\Entity\Punto $punto
	 */
	public function setPunto ($punto)
	{
		$this->punto = $punto;
	}

	/**
	 * Get id
	 *
	 * @return int
	 */
	public function getId ()
	{
		return $this->id;
	}

	/**
	 * Set nombre
	 *
	 * @param string $nombre
	 *
	 * @return Evento
	 */
	public function setNombre ($nombre)
	{
		$this->nombre = $nombre;

		return $this;
	}

	/**
	 * Get nombre
	 *
	 * @return string
	 */
	public function getNombre ()
	{
		return $this->nombre;
	}

	/**
	 * Set descripcion
	 *
	 * @param string $descripcion
	 *
	 * @return Evento
	 */
	public function setDescripcion ($descripcion)
	{
		$this->descripcion = $descripcion;

		return $this;
	}

	/**
	 * Get descripcion
	 *
	 * @return string
	 */
	public function getDescripcion ()
	{
		return $this->descripcion;
	}

	/**
	 * Set fecha
	 *
	 * @param \DateTime $fecha
	 *
	 * @return Evento
	 */
	public function setFecha ($fecha)
	{
		$this->fecha = $fecha;

		return $this;
	}

	/**
	 * Get fecha
	 *
	 * @return \DateTime
	 */
	public function getFecha ()
	{
		return $this->fecha;
	}

	/**
	 * Set hora
	 *
	 * @param \DateTime $hora
	 *
	 * @return Evento
	 */
	public function setHora ($hora)
	{
		$this->hora = $hora;

		return $this;
	}

	/**
	 * Get hora
	 *
	 * @return \DateTime
	 */
	public function getHora ()
	{
		return $this->hora;
	}


	/**
	 * @return \Turismo\TurismoBundle\Entity\Evento
	 */
	public function getFotos ()
	{
		return $this->fotos;
	}

	/**
	 * @param \Turismo\TurismoBundle\Entity\Evento $fotos
	 */
	public function setFotos ($fotos)
	{
		$this->fotos = $fotos;
	}

	public function __toString ()
	{
		return $this->id . ' - '. $this->nombre . ' - '. $this->fecha->format('Y-m-d');
	}
}

