<?php

namespace Turismo\TurismoBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\JoinColumn;
use Doctrine\ORM\Mapping\ManyToOne;

/**
 * FotoEvento
 *
 * @ORM\Entity
 */
class FotoEvento
{
	/**
	 * @var integer
	 *
	 * @ORM\Column(name="id", type="integer")
	 * @ORM\Id
	 * @ORM\GeneratedValue(strategy="AUTO")
	 */
	private $id;

	/**
	 * @var string
	 *
	 * @ORM\Column(name="descripcion", type="text", length=65535, nullable=true)
	 */
	private $descripcion;

	/**
	 * @var string
	 *
	 * @ORM\Column(name="imagen", type="string", length=200, nullable=true)
	 */
	private $imagen;

	/**
	 * @var boolean
	 *
	 * @ORM\Column(name="aprobado", type="boolean", nullable=false, options={"default":1})
	 */
	private $aprobado;


	/**
	 * @var Evento
	 *
	 * @ManyToOne(targetEntity="Turismo\TurismoBundle\Entity\Evento", inversedBy="fotos")
	 * @JoinColumn(name="evento_id", referencedColumnName="id")
	 *
	 */
	private $evento;


	/**
	 * Set id
	 *
	 * @param integer $id
	 *
	 * @return FotoEvento
	 */
	public function setId ($id)
	{
		$this->id = $id;

		return $this;
	}

	/**
	 * Get id
	 *
	 * @return integer
	 */
	public function getId ()
	{
		return $this->id;
	}

	/**
	 * Set descripcion
	 *
	 * @param string $descripcion
	 *
	 * @return FotoEvento
	 */
	public function setDescripcion ($descripcion)
	{
		$this->descripcion = $descripcion;

		return $this;
	}

	/**
	 * Get descripcion
	 *
	 * @return string
	 */
	public function getDescripcion ()
	{
		return $this->descripcion;
	}

	/**
	 * Set imagen
	 *
	 * @param string $imagen
	 *
	 * @return FotoEvento
	 */
	public function setImagen ($imagen)
	{
		$this->imagen = $imagen;

		return $this;
	}

	/**
	 * Get imagen
	 *
	 * @return string
	 */
	public function getImagen ()
	{
		return $this->imagen;
	}

	/**
	 * Set aprobado
	 *
	 * @param boolean $aprobado
	 *
	 * @return FotoEvento
	 */
	public function setAprobado ($aprobado)
	{
		$this->aprobado = $aprobado;

		return $this;
	}

	/**
	 * Get aprobado
	 *
	 * @return boolean
	 */
	public function getAprobado ()
	{
		return $this->aprobado;
	}


	/**
	 * @return \Turismo\TurismoBundle\Entity\Evento
	 */
	public function getEvento ()
	{
		return $this->evento;
	}

	/**
	 * @param \Turismo\TurismoBundle\Entity\Evento $evento
	 */
	public function setEvento ($evento)
	{
		$this->evento = $evento;
	}


}
