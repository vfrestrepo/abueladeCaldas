<?php
namespace Turismo\TurismoBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\OneToMany;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Punto
 *
 * @ORM\Entity
 */
class Punto
{
	/**
	 * @var integer
	 *
	 * @ORM\Column(name="id", type="integer")
	 * @ORM\Id
	 * @ORM\GeneratedValue(strategy="IDENTITY")
	 */
	private $id;

	/**
	 * @var string
	 *
	 * @ORM\Column(name="nombre", type="string", length=45, nullable=true)
	 */
	private $nombre;

	/**
	 * @var string
	 *
	 * @ORM\Column(name="descripcion", type="text", length=65535, nullable=true)
	 */
	private $descripcion;

	/**
	 * @var string
	 *
	 * @ORM\Column(name="nombre_presidente", type="string", length=45, nullable=true)
	 */
	private $nombrePresidente;

	/**
	 * @var \DateTime
	 *
	 * @ORM\Column(name="fecha_fundacion", type="date", nullable=true)
	 */
	private $fechaFundacion;

	/**
	 * @var string
	 *
	 * @ORM\Column(name="historia", type="text", length=65535, nullable=true)
	 */
	private $historia;

	/**
	 * @var string
	 *
	 * @ORM\Column(name="nombre_centro_educativo", type="string", length=45, nullable=true)
	 */
	private $nombreCentroEducativo;

	/**
	 * @var string
	 *
	 * @ORM\Column(name="lugar_cultural", type="string", length=45, nullable=true)
	 */
	private $lugarCultural;

	/**
	 * @var string
	 *
	 * @ORM\Column(name="actos_culturales", type="text", length=65535, nullable=true)
	 */
	private $actosCulturales;

	/**
	 * @var string
	 *
	 * @ORM\Column(name="delimitacion_geografica", type="text", length=65535, nullable=true)
	 */
	private $delimitacionGeografica;

	/**
	 * @var integer
	 *
	 * @ORM\Column(name="habitantes", type="integer", nullable=true)
	 */
	private $habitantes;

	/**
	 * @var string
	 *
	 * @ORM\Column(name="ruta", type="text", length=65535, nullable=true)
	 */
	private $ruta;

	/**
	 * @var boolean
	 *
	 * @ORM\Column(name="is_rural", type="boolean", nullable=true)
	 */
	private $isRural;

	/**
	 * @var Foto
	 *
	 * @OneToMany(targetEntity="Foto", mappedBy="punto")
	 *
	 */
	private $fotos;


	/**
	 * @var Evento
	 *
	 * @OneToMany(targetEntity="Evento", mappedBy="punto")
	 */
	private $eventos;


	/**
	 * Get id
	 *
	 * @return integer
	 */
	public function getId ()
	{
		return $this->id;
	}

	/**
	 * Set nombre
	 *
	 * @param string $nombre
	 *
	 * @return Punto
	 */
	public function setNombre ($nombre)
	{
		$this->nombre = $nombre;

		return $this;
	}

	/**
	 * Get nombre
	 *
	 * @return string
	 */
	public function getNombre ()
	{
		return $this->nombre;
	}

	/**
	 * Set descripcion
	 *
	 * @param string $descripcion
	 *
	 * @return Punto
	 */
	public function setDescripcion ($descripcion)
	{
		$this->descripcion = $descripcion;

		return $this;
	}

	/**
	 * Get descripcion
	 *
	 * @return string
	 */
	public function getDescripcion ()
	{
		return $this->descripcion;
	}

	/**
	 * Set nombrePresidente
	 *
	 * @param string $nombrePresidente
	 *
	 * @return Punto
	 */
	public function setNombrePresidente ($nombrePresidente)
	{
		$this->nombrePresidente = $nombrePresidente;

		return $this;
	}

	/**
	 * Get nombrePresidente
	 *
	 * @return string
	 */
	public function getNombrePresidente ()
	{
		return $this->nombrePresidente;
	}

	/**
	 * Set fechaFundacion
	 *
	 * @param \DateTime $fechaFundacion
	 *
	 * @return Punto
	 */
	public function setFechaFundacion ($fechaFundacion)
	{
		$this->fechaFundacion = $fechaFundacion;

		return $this;
	}

	/**
	 * Get fechaFundacion
	 *
	 * @return \DateTime
	 */
	public function getFechaFundacion ()
	{
		return $this->fechaFundacion;
	}

	/**
	 * Set historia
	 *
	 * @param string $historia
	 *
	 * @return Punto
	 */
	public function setHistoria ($historia)
	{
		$this->historia = $historia;

		return $this;
	}

	/**
	 * Get historia
	 *
	 * @return string
	 */
	public function getHistoria ()
	{
		return $this->historia;
	}

	/**
	 * Set nombreCentroEducativo
	 *
	 * @param string $nombreCentroEducativo
	 *
	 * @return Punto
	 */
	public function setNombreCentroEducativo ($nombreCentroEducativo)
	{
		$this->nombreCentroEducativo = $nombreCentroEducativo;

		return $this;
	}

	/**
	 * Get nombreCentroEducativo
	 *
	 * @return string
	 */
	public function getNombreCentroEducativo ()
	{
		return $this->nombreCentroEducativo;
	}

	/**
	 * Set lugarCultural
	 *
	 * @param string $lugarCultural
	 *
	 * @return Punto
	 */
	public function setLugarCultural ($lugarCultural)
	{
		$this->lugarCultural = $lugarCultural;

		return $this;
	}

	/**
	 * Get lugarCultural
	 *
	 * @return string
	 */
	public function getLugarCultural ()
	{
		return $this->lugarCultural;
	}

	/**
	 * Set actosCulturales
	 *
	 * @param string $actosCulturales
	 *
	 * @return Punto
	 */
	public function setActosCulturales ($actosCulturales)
	{
		$this->actosCulturales = $actosCulturales;

		return $this;
	}

	/**
	 * @return \Turismo\TurismoBundle\Entity\Foto
	 */
	public function getFotos ()
	{
		return $this->fotos;
	}

	/**
	 * @param \Turismo\TurismoBundle\Entity\Foto $fotos
	 */
	public function setFotos ($fotos)
	{
		$this->fotos = $fotos;
	}

	public function addFoto (Foto $foto)
	{
		$this->fotos->add ($foto);
	}

	public function removeFoto (Foto $foto)
	{
		$this->fotos->removeElement ($foto);
	}

	/**
	 * Get actosCulturales
	 *
	 * @return string
	 */
	public function getActosCulturales ()
	{
		return $this->actosCulturales;
	}

	/**
	 * Set delimitacionGeografica
	 *
	 * @param string $delimitacionGeografica
	 *
	 * @return Punto
	 */
	public function setDelimitacionGeografica ($delimitacionGeografica)
	{
		$this->delimitacionGeografica = $delimitacionGeografica;

		return $this;
	}

	/**
	 * Get delimitacionGeografica
	 *
	 * @return string
	 */
	public function getDelimitacionGeografica ()
	{
		return $this->delimitacionGeografica;
	}

	/**
	 * Set habitantes
	 *
	 * @param integer $habitantes
	 *
	 * @return Punto
	 */
	public function setHabitantes ($habitantes)
	{
		$this->habitantes = $habitantes;

		return $this;
	}

	/**
	 * Get habitantes
	 *
	 * @return integer
	 */
	public function getHabitantes ()
	{
		return $this->habitantes;
	}

	/**
	 * Set ruta
	 *
	 * @param string $ruta
	 *
	 * @return Punto
	 */
	public function setRuta ($ruta)
	{
		$this->ruta = $ruta;

		return $this;
	}

	/**
	 * Get ruta
	 *
	 * @return string
	 */
	public function getRuta ()
	{
		return $this->ruta;
	}

	/**
	 * @return boolean
	 */
	public function isIsRural ()
	{
		return $this->isRural;
	}

	/**
	 * @param boolean $isRural
	 */
	public function setIsRural ($isRural)
	{
		$this->isRural = $isRural;
	}

	/**
	 * @return \Turismo\TurismoBundle\Entity\Evento
	 */
	public function getEventos ()
	{
		return $this->eventos;
	}

	/**
	 * @param \Turismo\TurismoBundle\Entity\Foto $eventos
	 */
	public function setEventos ($eventos)
	{
		$this->eventos = $eventos;
	}

	public function __toString ()
	{
		return $this->getId () . ' - ' . $this->getNombre ();
	}


    /**
     * Constructor
     */
    public function __construct()
    {
        $this->fotos = new \Doctrine\Common\Collections\ArrayCollection();
        $this->eventos = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get isRural
     *
     * @return boolean
     */
    public function getIsRural()
    {
        return $this->isRural;
    }

    /**
     * Add evento
     *
     * @param \Turismo\TurismoBundle\Entity\Evento $evento
     *
     * @return Punto
     */
    public function addEvento(\Turismo\TurismoBundle\Entity\Evento $evento)
    {
        $this->eventos[] = $evento;

        return $this;
    }

    /**
     * Remove evento
     *
     * @param \Turismo\TurismoBundle\Entity\Evento $evento
     */
    public function removeEvento(\Turismo\TurismoBundle\Entity\Evento $evento)
    {
        $this->eventos->removeElement($evento);
    }
}
