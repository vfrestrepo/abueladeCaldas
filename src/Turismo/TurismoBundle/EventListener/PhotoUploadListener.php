<?php
/**
 * Created by PhpStorm.
 * User: Fabio
 * Date: 25/10/16
 * Time: 21:25
 */

namespace Turismo\TurismoBundle\EventListener;

use Symfony\Component\DependencyInjection\Container;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Doctrine\ORM\Event\LifecycleEventArgs;
use Doctrine\ORM\Event\PreUpdateEventArgs;
use Turismo\TurismoBundle\Controller\EventoController;
use Turismo\TurismoBundle\Entity\Foto;
use Turismo\TurismoBundle\FileUploader;
use Symfony\Component\HttpKernel\Controller;

class PhotoUploadListener
{
	private $uploader;

	public function __construct (FileUploader $uploader)
	{
		$this->uploader = $uploader;
	}



	public function prePersist (LifecycleEventArgs $args)
	{
		$entity = $args->getEntity ();

		$this->uploadFile ($entity);
	}

	public function preUpdate (PreUpdateEventArgs $args)
	{
		$entity = $args->getEntity ();

		$this->uploadFile ($entity);
	}

	private function uploadFile ($entity)
	{
		// upload only works for Product entities
		if (!$entity instanceof Foto) {
			return;
		}

		$file = $entity->getImagen ();

		// only upload new files
		if (!$file instanceof UploadedFile) {
			return;
		}

		$fileName = $this->uploader->upload ($file);
		$entity->setImagen ($fileName);
	}

//	public function postLoad(LifecycleEventArgs $args)
//	{
//		$entity = $args->getEntity();
//		if (!$entity instanceof Foto) {
//			return;
//		}
//
//		$fileName = $entity->getImagen();
//
//		$entity->setImagen(new File($this->uploader->getTargetDir().'/'.$fileName));
//	}
}
